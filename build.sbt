name := "stochasticsdistributions"

version := "0.2.3"

scalaVersion := "2.12.12"

organization := "com.gitlab.linde9821"

libraryDependencies ++= Seq(
  "org.scalactic" %% "scalactic" % "3.0.1",
  "org.scalatest" %% "scalatest" % "3.0.1" % "test"
)