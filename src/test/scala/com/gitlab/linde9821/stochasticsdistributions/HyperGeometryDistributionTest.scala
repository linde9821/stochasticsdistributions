package com.gitlab.linde9821.stochasticsdistributions

import org.scalatest.FunSuite
import scala.math.BigDecimal.double2bigDecimal

class HyperGeometryDistributionTest extends FunSuite {
    test("recursion works for P(X<=1) for H(20,6,2)") {
        val hyperGeometryDistribution: HyperGeometryDistribution = HyperGeometryDistribution(
            20, 6,
            2)

        val expected = HyperGeometryDistribution
                .densityFunction(hyperGeometryDistribution)(0) +
                HyperGeometryDistribution
                        .densityFunction(hyperGeometryDistribution)(1)
        assertResult(expected) {
            HyperGeometryDistribution
                    .distributionFunction(hyperGeometryDistribution)(1)
        }
    }

    test("recursion works for P(X<=0) for H(20,6,2)") {
        val hyperGeometryDistribution: HyperGeometryDistribution = HyperGeometryDistribution(
            20, 6,
            2)

        val expected = HyperGeometryDistribution
                .densityFunction(hyperGeometryDistribution)(0)
        assertResult(expected) {
            HyperGeometryDistribution
                    .distributionFunction(hyperGeometryDistribution)(0)
        }
    }

    test("high factorial runs") {
        val p = HyperGeometryDistribution
                .densityFunction(HyperGeometryDistribution(100, 8, 98))(5)
    }

    test("recursion Density works for P(X<=3) for H(20,6,2)") {
        val hyperGeometryDistribution: HyperGeometryDistribution = HyperGeometryDistribution(
            20, 6,
            2)

        val expected = HyperGeometryDistribution
                .densityFunction(hyperGeometryDistribution)(0) +
                HyperGeometryDistribution
                        .densityFunction(hyperGeometryDistribution)(1) +
                HyperGeometryDistribution
                        .densityFunction(hyperGeometryDistribution)(2) +
                HyperGeometryDistribution
                        .densityFunction(hyperGeometryDistribution)(3)
        assertResult(expected) {
            HyperGeometryDistribution
                    .distributionFunction(hyperGeometryDistribution)(3)
        }
    }

    test("variance and expectancy for H(198,18,4) and P(X=3), P(X<=3)") {
        val hyperGeometryDistribution: HyperGeometryDistribution = HyperGeometryDistribution(
            198,
            18, 4)

        val expected = (0.36363636363636, 0.32554432185258)
        assertResult(expected) {
            (HyperGeometryDistribution.expectation(hyperGeometryDistribution)
                    .setScale(14, BigDecimal.RoundingMode.HALF_UP).toDouble,
                    HyperGeometryDistribution
                            .variance(hyperGeometryDistribution)
                            .setScale(14, BigDecimal.RoundingMode.HALF_UP)
                            .toDouble)
        }
    }

    test("P(X=3) and P(X<=3) for H(198,18,4)") {
        val hyperGeometryDistribution: HyperGeometryDistribution = HyperGeometryDistribution(
            198,
            18, 4)
        val expectedProbabilities = (0.0023645679918341, 0.99995073816684)

        assertResult(expectedProbabilities) {
            (HyperGeometryDistribution
                    .densityFunction(hyperGeometryDistribution)(3)
                    .setScale(16, BigDecimal.RoundingMode.HALF_UP).toDouble,
                    HyperGeometryDistribution
                            .distributionFunction(hyperGeometryDistribution)(3)
                            .setScale(14, BigDecimal.RoundingMode.HALF_UP)
                            .toDouble)
        }
    }

    test("distributed works for P(X=2) for H(15,5,4)") {
        val hyperGeometryDistribution: HyperGeometryDistribution = HyperGeometryDistribution(
            15, 5,
            4)

        val expected = 0.3297
        assertResult(expected) {
            HyperGeometryDistribution
                    .densityFunction(hyperGeometryDistribution)(2)
                    .setScale(4, BigDecimal.RoundingMode.HALF_UP).toDouble
        }
    }
}
