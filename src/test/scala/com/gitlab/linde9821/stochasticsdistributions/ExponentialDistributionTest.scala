package com.gitlab.linde9821.stochasticsdistributions

import org.scalatest.FunSuite

class ExponentialDistributionTest extends FunSuite {

    val testDistribution: ExponentialDistribution = ExponentialDistribution(
        1.0 / 12.5)

    test("P(10 <= X <= 14)") {
        val expected = 0.1230491694941821

        assertResult(expected) {
            ExponentialDistribution
                    .distributionFunctionBetween(testDistribution)(10, 14)
        }
    }

    test("createFromExpectancy") {
        assertResult(testDistribution) {
            ExponentialDistribution.createFromExpectancy(12.5)
        }
    }

    test("P(X<=15.05)") {
        val expected = 0.7000081585912795

        assertResult(expected) {
            ExponentialDistribution
                    .distributionFunction(testDistribution)(15.05)
        }
    }

    test("expectancy") {
        val expected = 6.514657980456026

        assertResult(expected) {
            ExponentialDistribution.expectancy(ExponentialDistribution(0.1535))
        }
    }

    test("P(X>17)") {
        val expected = 0.2566607769535558

        assertResult(expected) {
            1 - ExponentialDistribution
                    .distributionFunction(testDistribution)(17)
        }
    }
}
