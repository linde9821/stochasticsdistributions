package com.gitlab.linde9821.stochasticsdistributions

case class ExponentialDistribution(lambda: Double) {
    override def equals(that: Any): Boolean = that match {
        case e: ExponentialDistribution => e.lambda == lambda
        case _ => false
    }
}

object ExponentialDistribution {

    import scala.math._

    def apply(lambda: Double): ExponentialDistribution = new ExponentialDistribution(
        lambda)

    def expectancy(exponentialDistribution: ExponentialDistribution): Double = {
        1.0 / exponentialDistribution.lambda
    }

    def createFromExpectancy(my: Double): ExponentialDistribution = {
        new ExponentialDistribution(1.0 / my)
    }

    def distributionFunctionBetween(exponentialDistribution: ExponentialDistribution)
                                   (x1: Double, x2: Double): Double = {
        if (x1 > x2) 0 else {
            def distribution = distributionFunction(exponentialDistribution)(
                _: Double): Double

            distribution(x2) - distribution(x1)
        }
    }

    def distributionFunction(exponentialDistribution: ExponentialDistribution)
                            (x: Double): Double = {
        if (x >= 0.0 && exponentialDistribution.lambda > 0.0) {
            1.0 - pow(E, -exponentialDistribution.lambda * x)
        } else 0.0
    }

    def densityFunction(exponentialDistribution: ExponentialDistribution)
                       (x: Double): Double = {
        if (x >= 0.0 && exponentialDistribution.lambda > 0.0) {
            exponentialDistribution.lambda *
                    pow(E, -exponentialDistribution.lambda * x)
        } else 0.0
    }

    def variance(exponentialDistribution: ExponentialDistribution): Double = {
        1 / pow(exponentialDistribution.lambda, 2)
    }
}