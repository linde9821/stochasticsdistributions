package com.gitlab.linde9821.stochasticsdistributions

import scala.annotation.tailrec

case class HyperGeometryDistribution(N: Int, M: Int, n: Int) {
    override def equals(that: Any): Boolean = ???
}

object HyperGeometryDistribution {
    def expectation(hyperGeometryDistribution: HyperGeometryDistribution): Double = {
        hyperGeometryDistribution.n.toDouble *
                hyperGeometryDistribution.M.toDouble /
                hyperGeometryDistribution.N.toDouble
    }

    def variance(hyperGeometryDistribution: HyperGeometryDistribution): Double = {
        val n = hyperGeometryDistribution.n.toDouble
        val N = hyperGeometryDistribution.N.toDouble
        val M = hyperGeometryDistribution.M.toDouble

        ((n * (N - n)) / (N - 1)) * (M / N) * (1 - M / N)
    }

    def distributionFunction(hyperGeometryDistribution: HyperGeometryDistribution)
                            (k: Int): BigDecimal = {
        @tailrec
        def distributionFunctionAccumulator(hyperGeometryDistribution: HyperGeometryDistribution)
                                           (k: Int,
                                            accumulator: BigDecimal): BigDecimal = k match {
            case 0 => densityFunction(hyperGeometryDistribution)(0) +
                    accumulator
            case k => {
                distributionFunctionAccumulator(hyperGeometryDistribution)(
                    k - 1,
                    densityFunction(hyperGeometryDistribution)(k) + accumulator)
            }
        }

        distributionFunctionAccumulator(hyperGeometryDistribution)(k, 0)
    }

    def densityFunction(hyperGeometryDistribution: HyperGeometryDistribution)
                       (k: Int): BigDecimal = {
        val maxValue = scala.math
                .min(hyperGeometryDistribution.M, hyperGeometryDistribution.n)
        k match {
            case k if 0 to maxValue contains k =>
                val n = hyperGeometryDistribution.n
                val N = hyperGeometryDistribution.N
                val M = hyperGeometryDistribution.M

                if (N - M < n - k) 0 else (nCr(M, k) * nCr(N - M, n - k)) /
                        nCr(N, n)
            case _ => 0
        }
    }

    private def nCr(n: Int, r: Int): BigDecimal = {
        if (n >= r) factorial(n) / (factorial(r) * factorial(n - r)) else 0
    }

    private def factorial(n: Long): BigDecimal = {
        @tailrec def factorialAccumulator(n: Long,
                                          accumulator: BigDecimal): BigDecimal = n match {
            case k if k <= 1 => accumulator
            case n => factorialAccumulator(n - 1, n * accumulator)
        }

        factorialAccumulator(n, 1)
    }
}
